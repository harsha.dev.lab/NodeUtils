const _ = require('lodash')

class NaturalDate {
  constructor () {
    // full months
    this.months = [
      'January', 'Febuary', 'March',
      'April', 'May', 'June',
      'July', 'August', 'September',
      'October', 'November', 'December'
    ]

    this.lowercaseMonths = this.months.map((val) => {
      return val.toLowerCase()
    })

    // month abbreviations
    this.monthAbbrs = [
      'Jan', 'Feb', 'Mar',
      'Apr', 'May', 'Jun',
      'Jul', 'Aug', 'Sep',
      'Oct', 'Nov', 'Dec'
    ]

    this.lowercaseMonthAbbrs = this.monthAbbrs.map((val) => {
      return val.toLowerCase()
    })

    // strings representing current time
    this.present = [
      'Present', 'Now'
    ]
  }

  // check if string is month
  parseMonth(str) {
    let ret = {
      'match': '',
      'type': ''
    }

    let tstr = _.deburr(str.toLowerCase())

    for (let i = 0; i < 12; i ++) {
      if (tstr === this.lowercaseMonths[i]) {
        ret.found = true
        ret.match = this.months[i]
        ret.type = 'full'
        break
      } else if (tstr.startsWith(this.lowercaseMonthAbbrs[i]) && this.lowercaseMonths[i].startsWith(tstr)) {
        ret.found = true
        ret.match = this.months[i]
        ret.type = 'abbr'
        break
      }
    }

    return ret
  }

  // compute regex
  crgx (regex, str) {
    let res = _.deburr(str).match(regex)
    if (res === null) {
      return []
    } else {
      return res
    }
  }

  // January 2001
  // Febuary 1999
  // March 2003
  // April 2000
  // May 1988
  // June 1960
  // July 1910
  // August 2018
  // September 2022
  // October 2099
  // November 1900
  // December 1999
  // and all abbreviated versions
  style1 (str) {
    let ret = {
      'match': [],
      'raw': [],
      'format': 'Month YYYY'
    }

    let regex = /(\b\w{3,9}[\s]*(?:[1]|[2])(?:[0]|[9])(?:[0-9]|[6-9])[0-9]\b)/g
    let res = this.crgx(regex, str)

    for (let i = 0; i < res.length; i++) {
      let toks = _.compact(res[i].split(' '))
      let check = this.parseMonth(toks[0])
      if (check.match !== '') {
        ret.match.push(`${check.match} ${toks[1]}`)
        ret.raw.push(res[i])
      }
    }

    return ret
  }

  style2 (str) {

  }
}

let nd = new NaturalDate()
let ret = nd.style1('January    2000 to Mark  2001 and October 2009 Jul 1999')
console.log(ret)